#!/bin/bash

set -ex

pip install -r cloudformation/requirements.txt

python cloudformation/stack_asg.py > stack_asg.json

cat stack_asg.json

python cloudformation/cfn_stack_update.py continousdeployment stack_asg.json
